// event listeners
document.addEventListener("deviceReady", connectToDatabase);
document.getElementById("saveButton").addEventListener("click", saveButtonPressed);
document.getElementById("showButton").addEventListener("click", showButtonPressed);


// my functions go here
//========================
function saveButtonPressed() {
  // debug:
  console.log("save button pressed!");
  alert("save button pressed!");
//1. get data from ui'
var n = document.getElementById("nameBox").value;
var d = document.getElementById("deptBox").value;

  console.log("Name : " +n);
  console.log("Dept : " +d);

  //insert into tables
  db.transaction(
  		function(tx){
  			tx.executeSql( "INSERT INTO employees(name, dept) VALUES(?,?)",
  			[n, d],
  			onSuccessExecuteSql,
  			onError )
  		},
  		onError,
  		onReadyTransaction
  	)
}

function showButtonPressed() {
  //debug:
  console.log("show button pressed!");
  alert("show button pressed!");

  // select from table 1. run query
  db.transaction(
  		function(tx){
  			tx.executeSql( "SELECT * FROM employees",
  			[],
  			displayResults,
  			onError )
  		},
  		onError,
  		onReadyTransaction
  	)
}
function displayResults( tx, results ){

  if(results.rows.length == 0) {
    alert("No records found");
    return false;
  }

  var row = "";
        for(var i=0; i<results.rows.length; i++) {
      document.getElementById("resultsSection").innerHTML +=
          "<p> Name: "
        +   results.rows.item(i).name
        + "<br>"
        + "Dept: "
        +   results.rows.item(i).dept
        + "</p>";

        }
}
//====================================


// connect to a database
var db = null;



function connectToDatabase() {
  console.log("device is ready - connecting to database");
  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
    //  (nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
    db = window.openDatabase("cestar", "1.0", "Database for Cestar College app", 2*1024*1024);
  }
  else {
    alert("mobile device detected");
    console.log("mobile device detected!");
    var databaseDetails = {"name":"cestar.db", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
  }

  if (!db) {
    alert("databse not opened!");
    return false;
  }
// creating tables..
db.transaction(
		function(tx){
			// Execute the SQL via a usually anonymous function
			// tx.executeSql( SQL string, arrary of arguments, success callback function, failure callback function)
			// To keep it simple I've added to functions below called onSuccessExecuteSql() and onFailureExecuteSql()
			// to be used in the callbacks
			tx.executeSql(
				"CREATE TABLE IF NOT EXISTS employees (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, dept TEXT)",
				[],
				onSuccessExecuteSql,
				onError
			)
		},
		onError,
		onReadyTransaction
	)
}



// some functions related to  the  creation of tables..!!
function onReadyTransaction( ){
  console.log( 'Transaction completed' )
}
function onSuccessExecuteSql( tx, results ){
  console.log( 'Execute SQL completed' )
}
function onError( err ){
  console.log( err )
}
